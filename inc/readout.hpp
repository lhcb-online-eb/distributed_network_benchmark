#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include <vector>
#include "../inc/common.hpp"
#include <tuple>
#include <glib.h>


#ifndef READOUT_HPP
#define READOUT_HPP
	
class ReadoutUnit
{
	public:
		//methods, constructors & destructors
		
		ReadoutUnit(int rank,int nodes);
		~ReadoutUnit();
		//run readout-associated operations
		void runReadout(size_t repeat, double *totalGbits);
	
	private:
		//methods
		
		//service upcoming Builder Units Requests by sending data back to them
		//void serviceRequests();
		void serviceRequests(int repeat, double *totalGbits);
		void serviceRequestsScheduled(int repeat, double *totalGbits);
		void serviceRequestsScheduled2(int repeat, double *totalGbits);
		//get next buffer entry in a round-robin fashion
		unsigned int getNextBuffIndex();
		//get next BU index - used for eval
		//unsigned int getNextBURank();
	
	private:
		//variables
		
		// MPI request struct for request message status control
		MPI_Request * reqRecvRequests;
		// MPI request struct for readout data transmission control
		MPI_Request * dataSendRequests;
		// MPIstatus struct for request message status control
		MPI_Status * reqRecvStatus;
		// MPI status struct for readout data transmission control
		MPI_Status * dataSendStatus;
		
		unsigned int requestsServicedNo;
		unsigned int requestsReceivedNo;
		
		bool * requestsServiced;


		int * grantedBUTokens;
		
		//pointer to hold for waitsome command to service complete requests
		int * validRequestsNos;
		
		//buffer index for data sending
		int buffInd;
		//buffer index for data sending
		unsigned int bUInd;
		//data buffer for sending data
		char ** dataBuffer = NULL;
		
		MpiCustomRequest *readRequestsMessages;
		
		//my rank
		unsigned int rank;
		//number of all processes
		unsigned int totalProcessesNo;
		//total number of buildout units
		unsigned int bUNumber;
		
		//class for scheduling and flow conntrol
		//contains utility functions for flow control
		class FlowController
		{
			public:
			//methods, constructors & destructors
				FlowController();
				// get next index for sending
				//if none applicable return -1
				int getScheduledReq();
				//delete a scheduled entry in requests vector under given index
				void deleteEntry(int index);
				//add new request to the vector of requests
				void pushBackNextReq(int sendRank,int eventNo);
				int getSenderRankAtInd(int ind);
				int getEventNoInd(int ind);
				//function invokek in a polling fashion to see if sufficient time has elapsed new tokens are available
				//updates tokens later achievable via gentTokens 
				void grantTokens();
				void grantTokensBu();
				//use single token for transmission
				void useToken();
				//get current number of valid tokens for transmission
				int getValidTokensNo();
				//get pending requests number
				int getPendingRequestsNo();
			private:
				//a vector of tuples containing requesting rank number, time and an event no
				std::vector<std::tuple<int,int64_t,int>> requests;
				int64_t time;
				int allTokens;
				
			
		};
		
		FlowController flowControler;

};
#endif



