#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include <sys/timerfd.h>

//#define MAX_NODES 128


#ifndef COMMON_HPP
#define COMMON_HPP

//how many processes there are per node
const unsigned int BUILDERS_N_READOUT_PER_NODE = 5;
//how many readout units there are per node
const unsigned int READOUT_PER_NODE = 1;
//how many buildout units there are per node
const unsigned int BUILDER_PER_NODE = 4;

//half a second interval value for builders random wait
const unsigned int RAND_MS = 1000;

//buffer size for single data portion in readout-to-builder units transmission
const size_t BUFFER_SIZE = 2*1024UL*1024UL;
//total buffer entries to be sent to builder units
const unsigned int BUFFER_COUNT = 256;


//const unsigned int ON_THE_GO_MESSAGES_BU = 8;
//
//const unsigned int REPEAT TRANSMISSIONS IN PERIOD 

//sending period in microseconds
const unsigned int PERIOD = 10000;


//how many times repeat the transmissions
const unsigned int REPEAT_FOR_EVAL = 1000;

//for how many repetitions display current throughput
const unsigned int PROBE_TP_QUANTA = 1000;

//maximal number of serviced transmissions at once
const int RU_MAX_TRANSMISSIONS_ON_THE_GO = 32;

//multiplication value to enable several requests at once
const unsigned int BU_MAX_REQUESTS_CREDITS = 8; 
//const unsigned int BU_MAX_REQUESTS_CREDITS = 1; 


//set to perform scheduled or unscheduled readout unit sending
const bool SCHEDULED= true; 


//parameter to set sending to local RUs
const bool SEND_TO_LOCAL = false; 


//number of tokens to send data
const int RU_TOKENS =64; 

const int RU_TOKENS_FOR_BU=16;
const int BU_PERIOD=10;

////////////

struct MpiCustomRequest
{
	int rank;
	int eventNo;
};








#endif



