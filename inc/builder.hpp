#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/common.hpp"

#ifndef BUILDER_HPP
#define BUILDER_HPP
	
class BuilderUnit
{

	public:
		//methods, constructors & destructors
		
		BuilderUnit(int rank,int allProcesses);
		~BuilderUnit();	
		//run builder-associated operations
		void runBuilder(size_t repeat, double *totalGbits);
	
	private:
		//methods
		
		void performReads(int repeat,double *totalGbits);
		//send requests for readouts
		void requestReads();
		//wait for readout data to arrive
		void waitForDataRecvCompletion();
		
		unsigned int getNextRURank();

		bool checkIfLocalRank(int rank);
		
		//get next available token
		//return -1 if none available
		int getValidCreditIndex();

		
	private:
		//variables
		
		//requests messages for data transmission sent to readout
		MpiCustomRequest * readRequests;

		
		bool * credits;
		// MPI request struct for request message status control
		MPI_Request * reqRequests;
		// MPI request struct for readout data transmission control
		MPI_Request * dataSendRequests;
		// MPIstatus struct for request message status control
		MPI_Status * reqStatus;
		// MPI status struct for readout data transmission control
		MPI_Status * dataSendStatus;
		
		//buffer index for data sending
		unsigned int ruInd;
		
		//buffer to receive data from readout
		//and to hold events
		//poinnter to pointer of pointer to event-indexed -> readout-indexed data arrays 
		char ** buffer = NULL;
		//char ** buffer = NULL;
		
		int *auxIndices;
		int outCount;
		
		//my rank
		unsigned int rank;
		//number of all processes
		unsigned int totalProcessesNo;
		//number of readout units
		unsigned int numberOfReadouts;
		unsigned int eventsCurrentlySent;


		int myLocalRuRanks[READOUT_PER_NODE];
		
		

};


#endif

