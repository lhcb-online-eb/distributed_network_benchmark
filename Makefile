BINDIR   = bin
OBJDIR   = obj

all: makedirs readout.o builder.o main.o 
	mpicxx -o bin/benchmark obj/readout.o obj/builder.o obj/main.o -Wall `pkg-config --libs glib-2.0` -O2
readout.o: src/readout.cpp
	mpicxx -o obj/readout.o src/readout.cpp `pkg-config --cflags glib-2.0` -c -O2 -Wall
builder.o: src/builder.cpp
	mpicxx -o obj/builder.o src/builder.cpp `pkg-config --cflags glib-2.0`  -c -O2 -Wall
main.o: src/main.cpp
	mpicxx -o obj/main.o src/main.cpp -c -O2 -Wall `pkg-config --cflags glib-2.0`
#common.o: src/common.cpp
#	mpicxx -o obj/common.o src/common.cpp -c -O2
#debug:
	#mpicxx -o bin/one_to_one src/one_to_one.cpp -DENABLE_DEBUG_PRINTS 
clean:
	rm bin/benchmark
	rm obj/*
makedirs:
ifneq ("$(wildcard $(OBJDIR))","")
	@echo "object folder exists"
else
	mkdir $(OBJDIR)
endif
ifneq ("$(wildcard $(BINDIR))","")
	@echo "binary folder exists"
else
	mkdir $(BINDIR)
endif

