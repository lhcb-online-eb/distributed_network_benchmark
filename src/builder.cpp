#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/builder.hpp"
#include <time.h>
#include "../inc/common.hpp"
#include <sys/timerfd.h>
#include <glib.h>

void BuilderUnit::runBuilder(size_t repeat, double *totalGbits)
{
	//printf("PROCESS RANK %d OUT OF %d PERFORMING BUILDER UNIT ACTIONS \n",rank,totalProcessesNo);
	
	performReads(repeat, totalGbits);

}


void BuilderUnit::performReads(int repeat, double *totalGbits)
{
	
	time_t t;
	srand((unsigned)time(&t)+(unsigned)rank);

	int allReqsNo=/*this->numberOfReadouts**/BU_MAX_REQUESTS_CREDITS;

	//printf("BUILDER %d IN REQUESTREAD: ALLREQSNO %d \n",rank,allReqsNo);

	int randval=rand()%RAND_MS;
	//printf("MY RANDVAL for sleep randomization %d \n",randval);
	usleep(randval);

	int64_t now,start,global_start,global_end;
	global_start = g_get_real_time();
	start=global_start;

	int serviced=0;

	unsigned int allReadoutsToReq;
	
	
	if(SEND_TO_LOCAL==true)
	{
		allReadoutsToReq=this->numberOfReadouts;
	}
	else
	{
		//subtract all local RUs
		allReadoutsToReq=this->numberOfReadouts-READOUT_PER_NODE;
	}
	
	//int total_iterations = this->numberOfReadouts * this->repeat;
	for(int i= 0; i<repeat;i++)
	{
		bool was_waiting=false;
		
		
		for(unsigned int j = 0 ; j < allReadoutsToReq ; j++)
		{
			int ind= getValidCreditIndex();
			//printf("BUILDER %d IN REQUESTREAD: IND: %d \n",rank, ind);
			
			if (ind == -1)
			{
			//	printf("BUILDER %d IN REQUESTREAD: RECLAIMING TOKEN \n",rank);
				
				was_waiting=true;
				MPI_Waitany(allReqsNo, this->dataSendRequests,&outCount,this->dataSendStatus);
				credits[outCount]=true;
				MPI_Request_free(&(reqRequests[outCount]));
				serviced++;
				ind=getValidCreditIndex();
				
			}
			
			credits[ind]=false;
			
			readRequests[ind].rank=rank;
			readRequests[ind].eventNo = i;
			int destination=getNextRURank();
			
			if(SEND_TO_LOCAL==false)
			{
				//get other ind while local rank
				while(checkIfLocalRank(destination)==true)
					destination=getNextRURank();
			}
			

		//	printf("BUILDER %d IN REQUESTREAD: SENDING TO RANK %d IND %d EVENT %d\n",rank,destination, ind, readRequests[ind].eventNo);
			MPI_Isend(&(readRequests[ind]),sizeof(MpiCustomRequest),MPI_CHAR,destination,rank,MPI_COMM_WORLD,&(reqRequests[ind]));
			MPI_Irecv((buffer[ind]),BUFFER_SIZE,MPI_CHAR,destination,MPI_ANY_TAG,MPI_COMM_WORLD,&(dataSendRequests[ind]));
		}
		
		now = g_get_real_time();
		
		
		if((i+1) % PROBE_TP_QUANTA == 0 && was_waiting)
		{
			long int bytes=BUFFER_SIZE*serviced;
			//printf("TIME: now: %ld start %ld\n",now,start);
			double tput_bytes_per_sec = (double)bytes * 1e6 / ((double)(now - start));
			double tput_gbits_per_sec = tput_bytes_per_sec/1024.0/1024.0/128.0;
			//printf("CURRENT CUMMULATED THROUGHPUT FOR BUILDER RANK %d: %lf Gbits/s\n",rank, tput_gbits_per_sec);
			//printf("THROUGHPUT FOR BUILDER RANK %u in Mbytes/s: %lf \n",rank, tput_mbtes_per_sec);
		}

	}
	
	int all_complete;
	do
	{
		MPI_Testall(allReqsNo, this->dataSendRequests,&all_complete, this->dataSendStatus);
	}
	while(!all_complete);
	
               
	for(int a=0;a!=allReqsNo;a++)
		MPI_Request_free(&(this->reqRequests[a]));

	global_end = g_get_real_time();

	int64_t cummulated_data= (int64_t)repeat *(int64_t)BUFFER_SIZE * allReadoutsToReq;
	int64_t cummulated_time=(global_end -global_start);


	
	double total_tput_bytes_per_sec=(double)cummulated_data* 1e6/(double)cummulated_time;
	double total_tput_mbtes_per_sec = total_tput_bytes_per_sec/1024.0/1024.0;
	double total_tput_gbits_per_sec = total_tput_mbtes_per_sec/1024.0*8.0;
	//printf("BUILDER RANK %d AFTER WAITALL \n",rank);

	//printf("***AVERAGE THROUGHPUT OF BUILDER RANK %d: %lf Gbits/s\n",rank, total_tput_gbits_per_sec);		
	
	*totalGbits=total_tput_gbits_per_sec;
}




BuilderUnit::BuilderUnit(int rank,int allProcesses)
{
	this->rank=rank;
	this->totalProcessesNo=allProcesses;

	this->eventsCurrentlySent=0;
	
	this->outCount=0;
	
	this->numberOfReadouts=this->totalProcessesNo/BUILDERS_N_READOUT_PER_NODE*READOUT_PER_NODE;
	
	//int allReqsNo=this->numberOfReadouts*BU_MAX_REQUESTS_CREDITS;
	int allReqsNo=/*this->numberOfReadouts**/BU_MAX_REQUESTS_CREDITS;
	
	this->readRequests=new MpiCustomRequest[allReqsNo];

	this->reqRequests=new MPI_Request[allReqsNo];
	this->dataSendRequests=new MPI_Request[allReqsNo];
	
	this->reqStatus=new MPI_Status[allReqsNo];
	this->dataSendStatus=new MPI_Status[allReqsNo];

	buffer = new char*[allReqsNo];

	auxIndices = new int[allReqsNo];

	this->credits= new bool[allReqsNo];

	for(int j = 0 ; j < allReqsNo ; j++)
		this->credits[j]=true;


	buffer = new char*[allReqsNo];
	for (int i = 0 ; i < allReqsNo ; i++) 
	{
		buffer[i] = (char*)memalign(2*1024*1024,BUFFER_SIZE);
	}

	for (int j = 0 ; j < allReqsNo ; j++)
	{
		reqRequests[j]=MPI_REQUEST_NULL;
		dataSendRequests[j]=MPI_REQUEST_NULL;
	}

	this->ruInd=BUILDER_PER_NODE;
	
	for(int i=0;i!=READOUT_PER_NODE;i++)
	{
			myLocalRuRanks[i]=(rank/BUILDERS_N_READOUT_PER_NODE)*BUILDERS_N_READOUT_PER_NODE+BUILDER_PER_NODE+i;
			
			//printf("BU RANK %d : MY LOCAL READOUT RANK: %d\n",this->rank,my_local_ru_ranks[i]);
	}

	//to randomize RUs accesses
	for(int i=0;i!=rank;i++)
		getNextRURank();
			
}


BuilderUnit::~BuilderUnit()
{	
	//int allReqsNo=this->numberOfReadouts*BU_MAX_REQUESTS_CREDITS;
	int allReqsNo=/*this->numberOfReadouts**/BU_MAX_REQUESTS_CREDITS;
	
	//free mem
	for (int i = 0 ; i < allReqsNo ; i++)
		free(buffer[i]);
		
	delete this->credits;
	delete [] this->buffer;
	delete [] this->reqRequests;
	delete [] this->dataSendRequests;
	delete [] this->reqStatus;
	delete [] this->dataSendStatus;
	delete [] this->readRequests;

}


unsigned int BuilderUnit::getNextRURank()
{
	unsigned int ind = ruInd;

	ruInd++;
	
	if(ruInd==totalProcessesNo)
		ruInd=BUILDER_PER_NODE;

	if((ruInd)%BUILDERS_N_READOUT_PER_NODE==0)
		ruInd+=BUILDER_PER_NODE;		
	
	return ind;
}

int BuilderUnit::getValidCreditIndex()
{
	int allReqsNo=/*this->numberOfReadouts**/BU_MAX_REQUESTS_CREDITS;
	
	for(int i=0;i!=allReqsNo;i++)
	{
		//printf("TOKEN at index %d: %d\n",i,tokens[i]);
		if(credits[i]==true)
			return i;
	}
	return -1;
		
}	

bool BuilderUnit::checkIfLocalRank(int rank)
{
	bool isLocal=false;
	for(int i=0;i!=READOUT_PER_NODE;i++)
	{
			if(myLocalRuRanks[i]==rank)
				isLocal=true;
	}
	return isLocal;
	
}
