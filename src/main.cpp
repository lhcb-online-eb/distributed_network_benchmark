#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/builder.hpp"
#include "../inc/readout.hpp"
#include "../inc/common.hpp"
#include <glib.h>
#include <limits>


void runCommunication(int rank, int nodes,size_t repeat,ReadoutUnit &ru, BuilderUnit &bu)
{

	
	double total_tput_gbits_per_sec,total_tput_gbits_per_sec_all;
	
	if (rank % BUILDERS_N_READOUT_PER_NODE >= BUILDER_PER_NODE)
	{	
		//ReadoutUnit ru(rank,nodes);
		ru.runReadout(repeat,&total_tput_gbits_per_sec);
	}	
	else
	{
		//BuilderUnit bu(rank,nodes);
		bu.runBuilder(repeat,&total_tput_gbits_per_sec);
	}
	

	
	double * all = new double[nodes];
	MPI_Gather(&total_tput_gbits_per_sec,1,MPI_DOUBLE,all,1,MPI_DOUBLE,0,MPI_COMM_WORLD);

	double glob_min_tp=std::numeric_limits<double>::infinity();
	double glob_max_tp=0.0;

	total_tput_gbits_per_sec_all=0.0;
	

	if(rank==0)
	{
		for(int a=0;a!=nodes;a++)
		{
			if(glob_max_tp<all[a])
				glob_max_tp=all[a];
			if(glob_min_tp>all[a] && all[a]!=0.0)
				glob_min_tp=all[a];
			total_tput_gbits_per_sec_all+=all[a];
		}
		int number_of_builders=nodes/BUILDERS_N_READOUT_PER_NODE*BUILDER_PER_NODE;
		total_tput_gbits_per_sec_all/=number_of_builders;

		printf("PROCESSES: %d AVERAGE %lf Gbits/s. MIN: %lf MAX: %lf \n",nodes, total_tput_gbits_per_sec_all,glob_min_tp,glob_max_tp);	
		//printf("***AVERAGE THROUGHPUT OF ALL BUILDER RANKS %lf Gbits/s. MIN: %lf MAX: %lf \n",total_tput_gbits_per_sec_all,glob_min_tp,glob_max_tp);				
		//printf("***AVERAGE THROUGHPUT OF BUILDER RANK %d: %lf Mbytes/s\n",rank, total_tput_mbtes_per_sec);	
	}
	delete all;
}


int main(int argc, char** argv) 
{
		
	//initialize
	MPI_Init(NULL, NULL);
	
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    char host_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(host_name, &name_len);

	if(world_size%BUILDERS_N_READOUT_PER_NODE!=0)
	{
		printf("A number of nodes must be divisible by %u. Processes per node should be exactly %u \n", BUILDERS_N_READOUT_PER_NODE,BUILDERS_N_READOUT_PER_NODE);
	}
	
//	if(rank==0)
//		printf("********************WORLD SIZE: %d\n",world_size);
	
	//printf("PROCESS ON HOST %s, RANK %d OUT OF %d WORLD SIZE\n", host_name, rank, world_size);

	ReadoutUnit ru(rank,world_size);
	BuilderUnit bu(rank,world_size);
	
	runCommunication(rank,world_size,REPEAT_FOR_EVAL,ru,bu);

	//printf("PROCESS RANK %d AFTER COMMUNICATION \n", rank);
	MPI_Finalize();

}
