#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include "../inc/readout.hpp"
#include <time.h>
#include "../inc/common.hpp"
#include "string.h"
#include <stdlib.h>

void ReadoutUnit::runReadout(size_t repeat, double *totalGbits)
{


//	printf("PROCESS RANK %d OUT OF %d PERFORMING READOUT UNIT ACTIONS FOR REPEAT %d \n",rank,totalProcessesNo,repeat);
	if(SCHEDULED==false)
		serviceRequests(repeat, totalGbits);
	else
		serviceRequestsScheduled(repeat, totalGbits);
}


ReadoutUnit::ReadoutUnit(int rank,int allProcesses )
{
	this->rank=rank;
	this->totalProcessesNo=allProcesses;
	this->bUNumber=BUILDER_PER_NODE*(totalProcessesNo/BUILDERS_N_READOUT_PER_NODE);
	
	this->dataBuffer = new char*[BUFFER_COUNT];
	
	this->reqRecvRequests=new MPI_Request[this->bUNumber];
	this->dataSendRequests=new MPI_Request[RU_MAX_TRANSMISSIONS_ON_THE_GO];
	
	this->reqRecvStatus=new MPI_Status[this->bUNumber];
	this->dataSendStatus=new MPI_Status[RU_MAX_TRANSMISSIONS_ON_THE_GO];

	
	//this->requestsServiced= new bool[this->bUNumber];	
	

	this->grantedBUTokens=new int[this->totalProcessesNo];

	for(int it=0;it!=this->totalProcessesNo;it++)
	{
		this->grantedBUTokens[it]=RU_TOKENS_FOR_BU;
	}
	
	this->validRequestsNos= new int[this->totalProcessesNo];
	
	this->readRequestsMessages = new MpiCustomRequest[this->totalProcessesNo];
	
	
	//allocate memory for buffer
	for (unsigned int i = 0 ; i < BUFFER_COUNT ; i++) 
	{
		dataBuffer[i] = (char*)memalign(2*1024*1024,BUFFER_SIZE);
	}
	
	//initialize requests structs
	for (unsigned int j = 0 ; j < this->bUNumber ; j++)
	{
		reqRecvRequests[j]=MPI_REQUEST_NULL;
	}

	for (unsigned int j = 0 ; j < RU_MAX_TRANSMISSIONS_ON_THE_GO ; j++)
	{
		dataSendRequests[j]=MPI_REQUEST_NULL;
	}
	// initialize buffer index
	this->buffInd=0;
	this->bUInd=1;

}




void ReadoutUnit::serviceRequestsScheduled2(int repeat, double *totalGbits)
{
	unsigned int allBUsToRecv;
	if(SEND_TO_LOCAL==true)
	{
		allBUsToRecv=this->bUNumber;
	}
	else
	{
		allBUsToRecv=this->bUNumber-BUILDER_PER_NODE;
	}
	
	for (unsigned int i = 0 ; i < allBUsToRecv ; i++)
	{
		MPI_Irecv(&(readRequestsMessages[i]),sizeof(MpiCustomRequest),MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(reqRecvRequests[i]));		
	}
	
	this->requestsServicedNo=0;
	this->requestsReceivedNo=0;
	int on_the_go=0;
		 
	unsigned int  all_iterations = allBUsToRecv*repeat;
	
	while(this->requestsServicedNo!=all_iterations)
	{
	//	printf("READOUT %d IN LOOP\n",rank);
		int request_ind;
		//take one 
		//TODO-UNCOMMENT AFTER EVAL
		MPI_Waitany(allBUsToRecv,reqRecvRequests,&request_ind,reqRecvStatus);
		
		int send_rank=readRequestsMessages[request_ind].rank;
		int event_no=readRequestsMessages[request_ind].eventNo;
		unsigned int buffer_ind=getNextBuffIndex();
		
		
		int indx;
		if(on_the_go==RU_MAX_TRANSMISSIONS_ON_THE_GO)
		{
		//	printf("READOUT %d IN WAITANY FOR EMPTY TRANSMISSION SLOTS\n",rank);
			MPI_Waitany(RU_MAX_TRANSMISSIONS_ON_THE_GO,dataSendRequests,&indx,dataSendStatus);
		}
		else
		{
			indx=on_the_go;
		}
		
		(*(int*)(dataBuffer[buffer_ind]))=event_no;
		//memset(dataBuffer[buffer_ind],event_no,BUFFER_SIZE);
		MPI_Isend((dataBuffer[buffer_ind]),BUFFER_SIZE,MPI_CHAR,send_rank,rank,MPI_COMM_WORLD,&(dataSendRequests[indx]));

		MPI_Irecv(&(readRequestsMessages[request_ind]),sizeof(MpiCustomRequest),MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(reqRecvRequests[request_ind]));
		if(on_the_go<RU_MAX_TRANSMISSIONS_ON_THE_GO)
			on_the_go++;
		
		this->requestsServicedNo++;
		
	}

	//printf("READOUT %d IN SERVICEREQUESTS BEFORE WAITALL\n",rank);
	MPI_Waitall(RU_MAX_TRANSMISSIONS_ON_THE_GO,dataSendRequests,dataSendStatus);
	/*
	for(int a=0;a!=MAX_TRANSMISSIONS_ON_THE_GO;a++)
	{
		printf("READOUT %d BUFFER AT FINALIZATION AT IND %d: %d \n",rank,a,(*(int*)(dataBuffer[a])));
	}
	*/
	
	
	for(unsigned int i=0;i!=allBUsToRecv;i++)
	{
		MPI_Cancel(&(reqRecvRequests[i]));
		MPI_Request_free(&(reqRecvRequests[i]));
	}
	//printf("READOUT %d IN SERVICEREQUESTS AFTER WAITALL\n",rank);
	
	*totalGbits=0.0;
}



void ReadoutUnit::serviceRequestsScheduled(int repeat, double *totalGbits)
{
	
	unsigned int allBUsToRecv;
	if(SEND_TO_LOCAL==true)
	{
		allBUsToRecv=this->bUNumber;
	}
	else
	{
		allBUsToRecv=this->bUNumber-BUILDER_PER_NODE;
	}
	
	for (unsigned int i = 0 ; i < allBUsToRecv ; i++)
	{
		MPI_Irecv(&(readRequestsMessages[i]),sizeof(MpiCustomRequest),MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(reqRecvRequests[i]));
	}
	
	this->requestsServicedNo=0;
	int on_the_go=0;
		 
	unsigned int  all_iterations = allBUsToRecv*repeat;

	while(this->requestsServicedNo!=all_iterations)
	{
		
		int request_ind;
		int recv_flag=false;

		MPI_Testany(allBUsToRecv,reqRecvRequests,&request_ind, &recv_flag,reqRecvStatus);

		flowControler.grantTokens();
		
		if(recv_flag==true)
		{
			int send_rank=readRequestsMessages[request_ind].rank;
			int event_no=readRequestsMessages[request_ind].eventNo;
			
			//printf("READOUT %d GOT REQUEST-PUTTING TO VECTOR BU RANK %d\n",rank,send_rank);

			flowControler.pushBackNextReq(send_rank,event_no);

			MPI_Irecv(&(readRequestsMessages[request_ind]),sizeof(MpiCustomRequest),MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(reqRecvRequests[request_ind]));	
			requestsReceivedNo++;
		}
		
		
		int valid_tokens=flowControler.getValidTokensNo();
		int valid_requests=flowControler.getPendingRequestsNo();
		
		int service_slots;
		if(valid_requests>valid_tokens)
			service_slots=valid_tokens;
		else
			service_slots=valid_requests;
			
		for(int ind=0;ind!=service_slots;ind++)
		{
			//printf("READOUT %d VALID REQUESTS %d VALID TOKENS %d SERVICE SLOTS %d\n",rank,valid_requests,valid_tokens,service_slots);
			int can_send=false;
			int indx;
			if(on_the_go<RU_MAX_TRANSMISSIONS_ON_THE_GO)
			{
				indx=on_the_go;
				can_send=true;
				on_the_go++;
			}
			else
			{
			//	MPI_Testany(MAX_TRANSMISSIONS_ON_THE_GO,dataSendRequests,&indx,&can_send,dataSendStatus);
			//	printf("READOUT %d IN WAITANY\n",rank);
			//	printf("RANK %d SIZE: BEFORE WAITANY %d\n",rank,flowControler.getPendingRequestsNo());
				MPI_Testany(RU_MAX_TRANSMISSIONS_ON_THE_GO,dataSendRequests,&indx,&can_send,dataSendStatus);
			//	printf("RANK %d SIZE: AFTER WAITANY %d\n",rank,flowControler.getPendingRequestsNo());
			}
		

					
			if(can_send)
			{
				unsigned int buffer_ind=getNextBuffIndex();
				
				int req_ind = flowControler.getScheduledReq();
				
				int send_rank=flowControler.getSenderRankAtInd(req_ind);
				int event_no=flowControler.getEventNoInd(req_ind);
				
			//	printf("READOUT %d IN SERVICEREQUESTS SENDING TO %d \n",rank,send_rank);
				
			//	printf("READOUT %d IN SERVICEREQUESTS SENDING TO %d REMAINING REQUESTS: %d TOKENS %d INDX %d SERVICE SLOTS %d\n",rank,send_rank,flowControler.getPendingRequestsNo(),flowControler.getValidTokensNo(),indx,service_slots);
			//	printf("READOUT %d IN SERVICEREQUESTS SENDING TO %d TOKENS: %d \n",rank,send_rank,flowControler.getValidTokensNo());
				flowControler.useToken();
				flowControler.deleteEntry(req_ind);
				//printf("RANK %d SIZE: BEFORE SEND %d\n",rank,flowControler.getPendingRequestsNo());
				
				(*(int*)(dataBuffer[buffer_ind]))=event_no;
				//memset(dataBuffer[buffer_ind],event_no,BUFFER_SIZE);
				MPI_Isend((dataBuffer[buffer_ind]),BUFFER_SIZE,MPI_CHAR,send_rank,rank,MPI_COMM_WORLD,&(dataSendRequests[indx]));						

				this->requestsServicedNo++;			
			}
		}	
	}

	//printf("READOUT %d IN SERVICEREQUESTS BEFORE WAITALL\n",rank);
	MPI_Waitall(RU_MAX_TRANSMISSIONS_ON_THE_GO,dataSendRequests,dataSendStatus);
	//clear all remainders
	for(unsigned int i=0;i!=allBUsToRecv;i++)
	{
		MPI_Cancel(&(reqRecvRequests[i]));
		MPI_Request_free(&(reqRecvRequests[i]));
	}
	//printf("READOUT %d IN SERVICEREQUESTS AFTER WAITALL\n",rank);
	
	*totalGbits=0.0;
}


void ReadoutUnit::serviceRequests(int repeat, double *totalGbits)
{
	unsigned int allBUsToRecv;
	if(SEND_TO_LOCAL==true)
	{
		allBUsToRecv=this->bUNumber;
	}
	else
	{
		allBUsToRecv=this->bUNumber-BUILDER_PER_NODE;
	}
	
	for (unsigned int i = 0 ; i < allBUsToRecv ; i++)
	{
		MPI_Irecv(&(readRequestsMessages[i]),sizeof(MpiCustomRequest),MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(reqRecvRequests[i]));		
	}
	
	this->requestsServicedNo=0;
	this->requestsReceivedNo=0;
	int on_the_go=0;
		 
	unsigned int  all_iterations = allBUsToRecv*repeat;
	
	while(this->requestsServicedNo!=all_iterations)
	{
	//	printf("READOUT %d IN LOOP\n",rank);
		int request_ind;
		//take one 
		//TODO-UNCOMMENT AFTER EVAL
		MPI_Waitany(allBUsToRecv,reqRecvRequests,&request_ind,reqRecvStatus);
		
		int send_rank=readRequestsMessages[request_ind].rank;
		int event_no=readRequestsMessages[request_ind].eventNo;
		unsigned int buffer_ind=getNextBuffIndex();
		
		
		int indx;
		if(on_the_go==RU_MAX_TRANSMISSIONS_ON_THE_GO)
		{
		//	printf("READOUT %d IN WAITANY FOR EMPTY TRANSMISSION SLOTS\n",rank);
			MPI_Waitany(RU_MAX_TRANSMISSIONS_ON_THE_GO,dataSendRequests,&indx,dataSendStatus);
		}
		else
		{
			indx=on_the_go;
		}
		
		(*(int*)(dataBuffer[buffer_ind]))=event_no;
		//memset(dataBuffer[buffer_ind],event_no,BUFFER_SIZE);
		MPI_Isend((dataBuffer[buffer_ind]),BUFFER_SIZE,MPI_CHAR,send_rank,rank,MPI_COMM_WORLD,&(dataSendRequests[indx]));

		MPI_Irecv(&(readRequestsMessages[request_ind]),sizeof(MpiCustomRequest),MPI_CHAR,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,&(reqRecvRequests[request_ind]));
		if(on_the_go<RU_MAX_TRANSMISSIONS_ON_THE_GO)
			on_the_go++;
		
		this->requestsServicedNo++;
		
	}

	//printf("READOUT %d IN SERVICEREQUESTS BEFORE WAITALL\n",rank);
	MPI_Waitall(RU_MAX_TRANSMISSIONS_ON_THE_GO,dataSendRequests,dataSendStatus);
	/*
	for(int a=0;a!=MAX_TRANSMISSIONS_ON_THE_GO;a++)
	{
		printf("READOUT %d BUFFER AT FINALIZATION AT IND %d: %d \n",rank,a,(*(int*)(dataBuffer[a])));
	}
	*/
	
	
	for(unsigned int i=0;i!=allBUsToRecv;i++)
	{
		MPI_Cancel(&(reqRecvRequests[i]));
		MPI_Request_free(&(reqRecvRequests[i]));
	}
	//printf("READOUT %d IN SERVICEREQUESTS AFTER WAITALL\n",rank);
	
	*totalGbits=0.0;
}


ReadoutUnit::~ReadoutUnit()
{

	//free buffer
	for (unsigned int i = 0 ; i < BUFFER_COUNT ; i++)
		free(dataBuffer[i]);
		
	delete [] this->dataBuffer;

	delete [] this->reqRecvRequests;
	delete [] this->reqRecvStatus;

	delete [] this->dataSendRequests;
	delete [] this->dataSendStatus;

	delete [] this->validRequestsNos;



}

unsigned int ReadoutUnit::getNextBuffIndex()
{
	unsigned int ind = buffInd;
	
	buffInd++;
	
	if(buffInd==BUFFER_COUNT)
		buffInd=0;
	
	return ind;
}


ReadoutUnit::FlowController::FlowController():requests(0)
{
	this->time=g_get_real_time();
	allTokens=RU_TOKENS;
	//requests = new std::vector<std::tuple<int,int64_t>>;
	// requests;
}

void ReadoutUnit::FlowController::pushBackNextReq(int sendRank,int eventNo)
{
	int64_t time  = g_get_real_time();
	requests.push_back(std::tuple<int,int64_t,int>(sendRank,time,eventNo));
}

void ReadoutUnit::FlowController::deleteEntry(int index)
{
	requests.erase(requests.begin()+index);
}

int ReadoutUnit::FlowController::getScheduledReq()
{
	if(requests.size()==0)
		return -1;
	else 
		return 0;

	
}	

void ReadoutUnit::FlowController::useToken()
{
	allTokens--;
}

int ReadoutUnit::FlowController::getValidTokensNo()
{
	return allTokens;
}


void ReadoutUnit::FlowController::grantTokens()
{
	int64_t current_time  = g_get_real_time();
	int64_t diff=current_time-time;
	if(diff>PERIOD)
	{
		allTokens+=RU_TOKENS;
		time=g_get_real_time();
	}
	
}

void ReadoutUnit::FlowController::grantTokensBu()
{
	int64_t current_time  = g_get_real_time();
	int64_t diff=current_time-time;
	//todo - multiply by RU/BU number ?
	if(diff>RU_TOKENS_FOR_BU)
	{
		for(int ind=0;ind!=;ind++)
			allTokens+=RU_TOKENS;
		time=g_get_real_time();
	}
	
}


int ReadoutUnit::FlowController::getPendingRequestsNo()
{
		return requests.size();
}	

int ReadoutUnit::FlowController::getSenderRankAtInd(int ind)
{
	return std::get<0>(requests[ind]);
}	

int ReadoutUnit::FlowController::getEventNoInd(int ind)
{
	return std::get<2>(requests[ind]);
}	
